from flask import Flask, request
import requests
import json
from flask_mysqldb import MySQL
import yaml

app = Flask(__name__)

# Database
db = yaml.load(open('db.yaml'))
app.config['MYSQL_HOST'] = db['mysql_host']
app.config['MYSQL_USER'] = db['mysql_user']
app.config['MYSQL_PASSWORD'] = db['mysql_password']
app.config['MYSQL_DB'] = db['mysql_db']

mysql = MySQL(app)


@app.route('/')
def hello_world():
    return 'Hello, World 2!'


def get_json(url):
    r = requests.get(url=url)
    r_json = json.loads(r.text)
    with app.app_context():
        conn = mysql.connect
        cur = conn.cursor()
        if r_json:
            if r_json['success']:
                user_data = r_json["data"]

                for x in user_data:

                    # USER
                    if x["user_id"]:
                        user_id = "NULL"
                        name = "NULL"
                        email = "NULL"
                        has_pic = "NULL"
                        pic_hash = "NULL"
                        active_flag = "NULL"
                        value = "NULL"
                        if x["user_id"].get("id"):
                            user_id = x["user_id"].get("id")
                        if x["user_id"].get("name"):
                            name = "'" + x["user_id"].get("name") + "'"
                        if x["user_id"].get("email"):
                            email = "'" + x["user_id"].get("email") + "'"
                        if x["user_id"].get("has_pic"):
                            has_pic = x["user_id"].get("has_pic")
                        if x["user_id"].get("pic_hash"):
                            pic_hash = "'" + x["user_id"].get("pic_hash") + "'"
                        if x["user_id"].get("active_flag"):
                            active_flag = x["user_id"].get("active_flag")
                        if x["user_id"].get("value"):
                            value = x["user_id"].get("value")
                        result = cur.execute(
                            "INSERT IGNORE INTO user(`id`,`name`,`email`,`has_pic`,`pic_hash`,`active_flag`,"
                            "`value`)VALUES(" + str(user_id) + ", " + name + ", " + email + ", "
                            + str(has_pic) + ", " + pic_hash + ", "
                            + str(active_flag) + ", " + str(value) + ")")
                        conn.commit()

                    # CREATOR_USER
                    if x["creator_user_id"]:
                        creator_user_id = "NULL"
                        name = "NULL"
                        email = "NULL"
                        has_pic = "NULL"
                        pic_hash = "NULL"
                        active_flag = "NULL"
                        value = "NULL"
                        if x["creator_user_id"].get("id"):
                            creator_user_id = x["creator_user_id"].get("id")
                        if x["creator_user_id"].get("name"):
                            name = "'" + x["creator_user_id"].get("name") + "'"
                        if x["creator_user_id"].get("email"):
                            email = "'" + x["creator_user_id"].get("email") + "'"
                        if x["creator_user_id"].get("has_pic"):
                            has_pic = x["creator_user_id"].get("has_pic")
                        if x["creator_user_id"].get("pic_hash"):
                            pic_hash = "'" + x["creator_user_id"].get("pic_hash") + "'"
                        if x["creator_user_id"].get("active_flag"):
                            active_flag = x["creator_user_id"].get("active_flag")
                        if x["creator_user_id"].get("value"):
                            value = x["creator_user_id"].get("value")
                        result = cur.execute(
                            "INSERT IGNORE INTO user(`id`,`name`,`email`,`has_pic`,`pic_hash`,`active_flag`,"
                            "`value`)VALUES(" + str(creator_user_id) + ", " + name + ", " + email + ", "
                            + str(has_pic) + ", " + pic_hash + ", "
                            + str(active_flag) + ", " + str(value) + ")")
                        conn.commit()

                    # PERSON
                    if x["person_id"]:
                        name = "NULL"
                        value = "NULL"
                        if x["person_id"].get("name"):
                            name = "'" + x["person_id"].get("name") + "'"
                        if x["person_id"].get("value"):
                            value = x["person_id"].get("value")
                        result = cur.execute(
                            "INSERT IGNORE INTO person(`name`,`value`)VALUES(" + name + ", " + str(value) + ")")
                        conn.commit()

                    # ORG
                    if x["org_id"]:
                        name = "NULL"
                        people_count = "NULL"
                        owner_id = "NULL"
                        address = "NULL"
                        cc_email = "NULL"
                        value = "NULL"
                        if x["org_id"].get("name"):
                            name = "'" + x["org_id"].get("name") + "'"
                        if x["org_id"].get("people_count"):
                            people_count = x["org_id"].get("people_count")
                        if x["org_id"].get("owner_id"):
                            owner_id = x["org_id"].get("owner_id")
                        if x["org_id"].get("address"):
                            address = "'" + x["org_id"].get("address") + "'"
                        if x["org_id"].get("cc_email"):
                            cc_email = "'" + x["org_id"].get("cc_email") + "'"
                        if x["org_id"].get("value"):
                            value = x["org_id"].get("value")
                        result = cur.execute(
                            "INSERT IGNORE INTO org_id (`name`,`people_count`,`owner_id`,`address`,`cc_email`,`value`)"
                            "VALUES(" + name + ", " + str(people_count) + ", " + str(owner_id) + ", " + address + ", "
                            + cc_email + ", " + str(value) + ")")
                        conn.commit()

                        # Regular data
                        id = x["id"] or "NULL"
                        stage_id = x["stage_id"] or "NULL"
                        title = x["title"] or "NULL"
                        value = x["value"] or "NULL"
                        currency = x["currency"] or "NULL"
                        add_time = x["add_time"] or "NULL"
                        update_time = x["update_time"] or "NULL"
                        stage_change_time = x["stage_change_time"] or "NULL"
                        active = x["active"] or "NULL"
                        deleted = x["deleted"] or "NULL"
                        status = x["status"] or "NULL"
                        probability = x["probability"] or "NULL"
                        next_activity_date = x["next_activity_date"] or "NULL"
                        next_activity_time = x["next_activity_time"] or "NULL"
                        next_activity_id = x["next_activity_id"] or "NULL"
                        last_activity_id = x["last_activity_id"] or "NULL"
                        last_activity_date = x["last_activity_date"] or "NULL"
                        lost_reason = x["lost_reason"] or "NULL"
                        visible_to = x["visible_to"] or "NULL"
                        close_time = x["close_time"] or "NULL"
                        pipeline_id = x["pipeline_id"] or "NULL"
                        won_time = x["won_time"] or "NULL"
                        first_won_time = x["first_won_time"] or "NULL"
                        lost_time = x["lost_time"] or "NULL"
                        products_count = x["products_count"] or "NULL"
                        files_count = x["files_count"] or "NULL"
                        notes_count = x["notes_count"] or "NULL"
                        followers_count = x["followers_count"] or "NULL"
                        email_messages_count = x["email_messages_count"] or "NULL"
                        activities_count = x["activities_count"] or "NULL"
                        done_activities_count = x["done_activities_count"] or "NULL"
                        undone_activities_count = x["undone_activities_count"] or "NULL"
                        reference_activities_count = x["reference_activities_count"] or "NULL"
                        participants_count = x["participants_count"] or "NULL"
                        expected_close_date = x["expected_close_date"] or "NULL"
                        ca4941e26dd9523c84ce39cd161a9919adc2c111 = x[
                                                                       "ca4941e26dd9523c84ce39cd161a9919adc2c111"] or "NULL"
                        ca4941e26dd9523c84ce39cd161a9919adc2c111_currency = x[
                                                                                "ca4941e26dd9523c84ce39cd161a9919adc2c111_currency"] or "NULL"
                        last_incoming_mail_time = x["last_incoming_mail_time"] or "NULL"
                        last_outgoing_mail_time = x["last_outgoing_mail_time"] or "NULL"
                        stage_order_nr = x["stage_order_nr"] or "NULL"
                        person_name = x["person_name"] or "NULL"
                        org_name = x["org_name"] or "NULL"
                        next_activity_subject = x["next_activity_subject"] or "NULL"
                        next_activity_type = x["next_activity_type"] or "NULL"
                        next_activity_duration = x["next_activity_duration"] or "NULL"
                        next_activity_note = x["next_activity_note"] or "NULL"
                        formatted_value = x["formatted_value"] or "NULL"
                        weighted_value = x["weighted_value"] or "NULL"
                        formatted_weighted_value = x["formatted_weighted_value"] or "NULL"
                        weighted_value_currency = x["weighted_value_currency"] or "NULL"
                        rotten_time = x["rotten_time"] or "NULL"
                        owner_name = x["owner_name"] or "NULL"
                        cc_email = x["cc_email"] or "NULL"
                        org_hidden = x["org_hidden"] or "NULL"
                        person_hidden = x["person_hidden"] or "NULL"
                        creator_user_id = x["creator_user_id"].get("id") or "NULL"
                        user_id = x["user_id"].get("id") or "NULL"
                        if x["org_id"]:
                            result = cur.execute("SELECT id FROM org_id where name = '" + x["org_id"].get("name") + "'")
                            conn.commit()
                            if result > 0:
                                org_id = cur.fetchall()[0][0]
                            else:
                                org_id = "NULL"
                        else:
                            org_id = "NULL"

                        if x["person_id"]:
                            result = cur.execute(
                                "SELECT id FROM person where name = '" + x["person_id"].get("name") + "'")
                            conn.commit()
                            if result > 0:
                                person_id = cur.fetchall()[0][0]
                            else:
                                person_id = "NULL"
                        else:
                            person_id = "NULL"

                        result = cur.execute(
                            "INSERT IGNORE INTO deal(`id`,`stage_id`,`title`,`value`,`currency`,`add_time`,"
                            "`update_time`,`stage_change_time`,`active`,`deleted`,`status`,`probability`,"
                            "`next_activity_date`,`next_activity_time`,`next_activity_id`,`last_activity_id`,"
                            "`last_activity_date`,`lost_reason`,`visible_to`,`close_time`,`pipeline_id`,"
                            "`won_time`,`first_won_time`,`lost_time`,`products_count`,`files_count`,"
                            "`notes_count`,`followers_count`,`email_messages_count`,`activities_count`"
                            ",`done_activities_count`,`undone_activities_count`,`reference_activities_count`"
                            ",`participants_count`,`expected_close_date`,"
                            "`ca4941e26dd9523c84ce39cd161a9919adc2c111`,"
                            "`ca4941e26dd9523c84ce39cd161a9919adc2c111_currency`,`last_incoming_mail_time`,"
                            "`last_outgoing_mail_time`,`stage_order_nr`,`person_name`,`org_name`,"
                            "`next_activity_subject`,`next_activity_type`,`next_activity_duration`,"
                            "`next_activity_note`,`formatted_value`,`weighted_value`,`formatted_weighted_value`,"
                            "`weighted_value_currency`,`rotten_time`,`owner_name`,`cc_email`,`org_hidden`,"
                            "`person_hidden`,`creator_user_id`,`user_id`,`org_id`,`person_id`)VALUES("
                            + str(id) + ", " + str(stage_id) + ", '" + str(title) + "', "
                            + str(value) + ", '" + str(currency) + "', '" + str(add_time) + "', '"
                            + str(update_time) + "', '" + str(stage_change_time) + "', '" + str(active) + "', '"
                            + str(deleted) + "', '" + str(status) + "', " + str(probability) + ", '"
                            + str(next_activity_date) + "', '" + str(next_activity_time) + "', "
                            + str(next_activity_id) + ", "
                            + str(last_activity_id) + ", '" + str(last_activity_date) + "', '" + str(lost_reason) + "', "
                            + str(visible_to) + ", '" + str(close_time) + "', " + str(pipeline_id) + ", '"
                            + str(won_time) + "', '" + str(first_won_time) + "', '" + str(lost_time) + "', "
                            + str(products_count) + ", " + str(files_count) + ", " + str(notes_count) + ", "
                            + str(followers_count) + ", " + str(email_messages_count) + ", " + str(
                                activities_count) + ", "
                            + str(done_activities_count) + ", " + str(undone_activities_count) + ", "
                            + str(reference_activities_count) + ", "
                            + str(participants_count) + ", '" + str(expected_close_date) + "', '"
                            + str(ca4941e26dd9523c84ce39cd161a9919adc2c111) + "', '"
                            + str(ca4941e26dd9523c84ce39cd161a9919adc2c111_currency) + "', '"
                            + str(last_incoming_mail_time) + "', '" + str(last_outgoing_mail_time) + "', '"
                            + str(stage_order_nr) + "', '" + str(person_name) + "', '" + str(org_name) + "', '"
                            + str(next_activity_subject) + "', '" + str(next_activity_type) + "', '"
                            + str(next_activity_duration) + "', '"
                            + str(next_activity_note) + "', '" + str(formatted_value) + "', '" + str(
                                weighted_value) + "', '"
                            + str(formatted_weighted_value) + "', '" + str(weighted_value_currency) + "', '"
                            + str(rotten_time) + "', '"
                            + str(owner_name) + "', '" + str(cc_email) + "', " + str(org_hidden) + ", "
                            + str(person_hidden) + ", " + str(creator_user_id) + ", " + str(user_id) + ", "
                            + str(org_id) + ", " + str(person_id) + ");")
                        conn.commit()

                        # result = cur.execute(

                        #     "INSERT IGNORE INTO org_id (`name`,`people_count`,`owner_id`,`address`,`cc_email`,`value`)"
                        #     "VALUES(" + name + ", " + str(people_count) + ", " + str(
                        #         owner_id) + ", " + address + ", "
                        #     + cc_email + ", " + str(value) + ")")
                        #

        cur.close()


@app.route("/read")
def read():
    get_json(
        "https://api.pipedrive.com/v1/deals?status=all_not_deleted&start=0&api_token=8ac6d7bb298314f891221b9b42b6eeaabffd0d15")
    return "Read it!"
