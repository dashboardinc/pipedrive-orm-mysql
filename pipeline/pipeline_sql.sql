-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema pipeline_api
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema pipeline_api
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `pipeline_api` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE `pipeline_api` ;

-- -----------------------------------------------------
-- Table `pipeline_api`.`activity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pipeline_api`.`activity` (
  `id` INT(11) NOT NULL,
  `company_id` INT(11) NULL DEFAULT NULL,
  `user_id` INT(11) NULL DEFAULT NULL,
  `done` TINYINT(4) NULL DEFAULT NULL,
  `type_id` INT(11) NULL DEFAULT NULL,
  `reference_type` VARCHAR(45) NULL DEFAULT NULL,
  `reference_id` INT(11) NULL DEFAULT NULL,
  `due_date` DATETIME NULL DEFAULT NULL,
  `duration` VARCHAR(45) NULL DEFAULT NULL,
  `add_time` DATETIME NULL DEFAULT NULL,
  `marked_as_done_time` DATETIME NULL DEFAULT NULL,
  `last_notification_time` DATETIME NULL DEFAULT NULL,
  `last_notification_user_id` INT(11) NULL DEFAULT NULL,
  `notification_language_id` INT(11) NULL DEFAULT NULL,
  `subject` VARCHAR(45) NULL DEFAULT NULL,
  `org_id` INT(11) NULL DEFAULT NULL,
  `person_id` INT(11) NULL DEFAULT NULL,
  `deal_id` INT(11) NULL DEFAULT NULL,
  `active_flag` TINYINT(4) NULL DEFAULT NULL,
  `update_time` DATETIME NULL DEFAULT NULL,
  `update_user_id` INT(11) NULL DEFAULT NULL,
  `gcal_event_id` INT(11) NULL DEFAULT NULL,
  `google_calendar_id` INT(11) NULL DEFAULT NULL,
  `google_calendar_etag` VARCHAR(45) NULL DEFAULT NULL,
  `source_timezone` VARCHAR(45) NULL DEFAULT NULL,
  `rec_rule` VARCHAR(45) NULL DEFAULT NULL,
  `rec_rule_extension` VARCHAR(45) NULL DEFAULT NULL,
  `rec_master_activity_id` INT(11) NULL DEFAULT NULL,
  `note` VARCHAR(45) NULL DEFAULT NULL,
  `created_by_user_id` INT(11) NULL DEFAULT NULL,
  `participants` VARCHAR(45) NULL DEFAULT NULL,
  `series` VARCHAR(45) NULL DEFAULT NULL,
  `org_name` VARCHAR(45) NULL DEFAULT NULL,
  `person_name` VARCHAR(45) NULL DEFAULT NULL,
  `deal_title` VARCHAR(45) NULL DEFAULT NULL,
  `owner_name` VARCHAR(45) NULL DEFAULT NULL,
  `person_dropbox_bcc` VARCHAR(45) NULL DEFAULT NULL,
  `deal_dropbox_bcc` VARCHAR(45) NULL DEFAULT NULL,
  `assigned_to_user_id` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `pipeline_api`.`activity_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pipeline_api`.`activity_type` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `pipeline_api`.`deal`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pipeline_api`.`deal` (
  `id` INT(11) NOT NULL,
  `stage_id` INT(11) NULL DEFAULT NULL,
  `title` VARCHAR(45) NULL DEFAULT NULL,
  `value` INT(11) NULL DEFAULT NULL,
  `currency` VARCHAR(45) NULL DEFAULT NULL,
  `add_time` DATETIME NULL DEFAULT NULL,
  `update_time` DATETIME NULL DEFAULT NULL,
  `stage_change_time` DATETIME NULL DEFAULT NULL,
  `active` VARCHAR(45) NULL DEFAULT NULL,
  `deleted` VARCHAR(45) NULL DEFAULT NULL,
  `status` VARCHAR(45) NULL DEFAULT NULL,
  `probability` VARCHAR(45) NULL DEFAULT NULL,
  `next_activity_date` DATETIME NULL DEFAULT NULL,
  `next_activity_time` DATETIME NULL DEFAULT NULL,
  `next_activity_id` INT(11) NULL DEFAULT NULL,
  `last_activity_id` INT(11) NULL DEFAULT NULL,
  `last_activity_date` DATETIME NULL DEFAULT NULL,
  `lost_reason` VARCHAR(45) NULL DEFAULT NULL,
  `visible_to` INT(11) NULL DEFAULT NULL,
  `close_time` DATETIME NULL DEFAULT NULL,
  `pipeline_id` INT(11) NULL DEFAULT NULL,
  `won_time` DATETIME NULL DEFAULT NULL,
  `first_won_time` DATETIME NULL DEFAULT NULL,
  `lost_time` DATETIME NULL DEFAULT NULL,
  `products_count` INT(11) NULL DEFAULT NULL,
  `files_count` INT(11) NULL DEFAULT NULL,
  `notes_count` INT(11) NULL DEFAULT NULL,
  `followers_count` INT(11) NULL DEFAULT NULL,
  `email_messages_count` INT(11) NULL DEFAULT NULL,
  `activities_count` INT(11) NULL DEFAULT NULL,
  `done_activities_count` INT(11) NULL DEFAULT NULL,
  `undone_activities_count` INT(11) NULL DEFAULT NULL,
  `reference_activities_count` INT(11) NULL DEFAULT NULL,
  `participants_count` INT(11) NULL DEFAULT NULL,
  `expected_close_date` VARCHAR(45) NULL DEFAULT NULL,
  `ca4941e26dd9523c84ce39cd161a9919adc2c111` VARCHAR(45) NULL DEFAULT NULL,
  `ca4941e26dd9523c84ce39cd161a9919adc2c111_currency` VARCHAR(45) NULL DEFAULT NULL,
  `last_incoming_mail_time` DATETIME NULL DEFAULT NULL,
  `last_outgoing_mail_time` DATETIME NULL DEFAULT NULL,
  `stage_order_nr` VARCHAR(45) NULL DEFAULT NULL,
  `person_name` VARCHAR(45) NULL DEFAULT NULL,
  `org_name` VARCHAR(45) NULL DEFAULT NULL,
  `next_activity_subject` VARCHAR(45) NULL DEFAULT NULL,
  `next_activity_type` VARCHAR(45) NULL DEFAULT NULL,
  `next_activity_duration` VARCHAR(45) NULL DEFAULT NULL,
  `next_activity_note` VARCHAR(45) NULL DEFAULT NULL,
  `formatted_value` VARCHAR(45) NULL DEFAULT NULL,
  `weighted_value` VARCHAR(45) NULL DEFAULT NULL,
  `formatted_weighted_value` VARCHAR(45) NULL DEFAULT NULL,
  `weighted_value_currency` VARCHAR(45) NULL DEFAULT NULL,
  `rotten_time` VARCHAR(45) NULL DEFAULT NULL,
  `owner_name` VARCHAR(45) NULL DEFAULT NULL,
  `cc_email` VARCHAR(45) NULL DEFAULT NULL,
  `org_hidden` TINYINT(4) NULL DEFAULT NULL,
  `person_hidden` TINYINT(4) NULL DEFAULT NULL,
  `creator_user_id` INT(11) NULL DEFAULT NULL,
  `user_id` INT(11) NULL DEFAULT NULL,
  `org_id` VARCHAR(45) NULL DEFAULT NULL,
  `person_id` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `pipeline_api`.`deal_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pipeline_api`.`deal_status` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `pipeline_api`.`email_person`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pipeline_api`.`email_person` (
  `id` INT(11) NOT NULL,
  `person_id` INT(11) NULL DEFAULT NULL,
  `email` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `pipeline_api`.`org_id`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pipeline_api`.`org_id` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  `people_count` INT(11) NULL DEFAULT NULL,
  `owner_id` INT(11) NULL DEFAULT NULL,
  `address` VARCHAR(45) NULL DEFAULT NULL,
  `cc_email` VARCHAR(45) NULL DEFAULT NULL,
  `value` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 98
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `pipeline_api`.`person`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pipeline_api`.`person` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `value` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `name`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 67
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `pipeline_api`.`phone_person`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pipeline_api`.`phone_person` (
  `id` INT(11) NOT NULL,
  `person_id` INT(11) NULL DEFAULT NULL,
  `label` VARCHAR(45) NULL DEFAULT NULL,
  `value` INT(11) NULL DEFAULT NULL,
  `primary` TINYINT(4) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `pipeline_api`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pipeline_api`.`user` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  `email` VARCHAR(45) NULL DEFAULT NULL,
  `has_pic` TINYINT(4) NULL DEFAULT NULL,
  `pic_hash` VARCHAR(45) NULL DEFAULT NULL,
  `active_flag` TINYINT(4) NULL DEFAULT NULL,
  `value` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
